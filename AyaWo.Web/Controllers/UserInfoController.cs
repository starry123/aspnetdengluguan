﻿using AyaWo.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;

namespace AyaWo.Web.Controllers
{
    public class UserInfoController : MvcControllerBase
    {
        // GET: UserInfo
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetUserInfo()
        {
            var userID = Operator.GetUserInfo().UserID;
            var q = UsersInfoService.GetUserInfo(userID);
            return Json(q, JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// 判断账户名称的唯一性
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="account"></param>
        /// <returns></returns>
        public ActionResult AccountOnly(string account)
        {
            var userID = Operator.GetUserInfo().UserID;
            var q = UsersInfoService.AccountOnly(userID, account);
            return Json(q, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 判断昵称的唯一性
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="account"></param>
        /// <returns></returns>
        public ActionResult NickNameOnly(string nickName)
        {
            var userID = Operator.GetUserInfo().UserID;
            var q = UsersInfoService.NickNameOnly(userID, nickName);
            return Json(q, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SaveEntity(string obj)
        {
            UsersInfoSys entity = JsonConvert.DeserializeObject<UsersInfoSys>(obj);
            var userID = Operator.GetUserInfo().UserID;
            entity.UserID = userID;
            var q = UsersInfoService.SaveEntity(entity);
            return Json(q, JsonRequestBehavior.AllowGet);
        }

    }
}
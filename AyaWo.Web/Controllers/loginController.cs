﻿using AyaWo.Business;
using AyaWo.Util;
using AyaWo.Util.Cache;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AyaWo.Web.Controllers
{
    [HandlerLogin(FilterMode.Ignore)]
    public class loginController : MvcControllerBase
    {
        // GET: login
        public ActionResult Index(string backUrl)
        {
            if(backUrl.IsNullOrWhiteSpace())
                backUrl= "/Home";
            ViewData["backUrl"] = backUrl;
            return View();
        }

        /// <summary>
        /// 生成验证码
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult VerifyCode()
        {
            return File(new VerifyCode().GetVerifyCode(), @"image/Gif");
        }


        /// <summary>
        /// 安全退出
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        [HandlerLogin(FilterMode.Enforce)]
        public ActionResult OutLogin()
        {
            var userInfo = Operator.GetUserInfo();
            LogInfo logEntity = new LogInfo();
            logEntity.CategoryId = 1;

            logEntity.OperateType = "退出登陆";
            logEntity.OperateAccount = userInfo.Account + "(" + userInfo.NickName + ")";
            logEntity.OperateUserId = userInfo.UserID;
            logEntity.ExecuteResult = 1;
            logEntity.ExecuteResultJson = "退出系统";
            logEntity.Module = "登陆模块";
            logEntity.WriteLog();
            Session.Abandon();                                          //清除当前会话
            Session.Clear();                                            //清除当前浏览器所有Session
            Operator.OutLog();
            return Success("退出系统");
        }


        /// <summary>
        /// 登录验证
        /// </summary>
        /// <param name="username">用户名</param>
        /// <param name="password">密码</param>
        /// <param name="verifycode">验证码</param>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        [HandlerValidateAntiForgeryToken]
        public ActionResult CheckLogin(string username, string password, string verifycode)
        {
            verifycode = Md5Helper.Encrypt(verifycode.ToLower(), 16);
            if (Session["session_verifycode"].IsEmpty() || verifycode != Session["session_verifycode"].ToString())
            {
                return Fail("验证码错误");
            }


            #region 内部账户验证
            UsersInfoSys userEntity = UsersInfoService.CheckLogin(username, password);

            #region 写入日志
            LogInfo logEntity = new LogInfo();
            logEntity.LogId = Guid.NewGuid().ToString();

            logEntity.CategoryId = 1;
            logEntity.OperateTime = DateTime.Now;
            logEntity.Browser = Net.Browser;
            logEntity.Host = WebHelper.Host;
            logEntity.IPAddress = Net.Ip;
            logEntity.OperateType ="用户登陆";
            logEntity.OperateAccount = username + "(" + userEntity.NickName + ")";
            logEntity.OperateUserId = !string.IsNullOrEmpty(userEntity.UserID) ? userEntity.UserID : username;
            logEntity.Module = "登陆模块";
            #endregion

            if (!userEntity.LoginOk)//登录失败
            {
                //写入日志
                logEntity.ExecuteResult = 0;
                logEntity.ExecuteResultJson = "登录失败:" + userEntity.LoginMsg;
                logEntity.WriteLog();
                 
                return Fail(userEntity.LoginMsg);
            }
            else
            {
                var guid= Guid.NewGuid().ToString();

                WebHelper.WriteCookie("ayawo_value", guid);
                CacheFactory.Cache().Write<UsersInfoSys>(guid, userEntity,DateTime.Now.AddHours(6));

                //写入日志
                logEntity.ExecuteResult = 1;
                logEntity.ExecuteResultJson = "登录成功";
                logEntity.WriteLog();
                return Success("登录成功");
            }
            #endregion
        }
    }
}
﻿using AyaWo.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AyaWo.Web.Controllers
{
    public class LoginLogController : MvcControllerBase
    {
        // GET: LoginLog
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult LoginLog(int? page)
        {
            if (page==null||page == 0)
                page = 1; 
            var userInfo = Operator.GetUserInfo();
          var q=  LogInfoServices.LoginLog(userInfo.UserID,page??1);
            return Json(q, JsonRequestBehavior.AllowGet);
        }
    }
}
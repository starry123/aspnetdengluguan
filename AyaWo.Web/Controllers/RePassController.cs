﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AyaWo.Util;
using System.Threading.Tasks;
using AyaWo.Business;

namespace AyaWo.Web.Controllers
{
    public class RePassController : MvcControllerBase
    {
        // GET: RePass
        public ActionResult Index()
        {
            ViewData["Account"] = Operator.GetUserInfo().Account;
            return View();
        }

        /// <summary>
        /// 密码校验
        /// </summary>
        /// <param name="ypass"></param>
        /// <returns></returns>
        public ActionResult yuanPassWorrd(string ypass)
        {
            var pass = Operator.GetUserInfo().Password;
            if (pass.Equals(ypass))
                return Json(true, JsonRequestBehavior.AllowGet);
            else
                return Json(false,JsonRequestBehavior.AllowGet);
        }

        public ActionResult SaveData(string ypass,string pass,string cpass)
        {
            if (ypass.IsNullOrWhiteSpace())
                return Fail("原密码不能为空...");
            if (pass.Equals(ypass))
                return Fail("现在的密码不能和原密码相同");
            if(!pass.Equals(cpass))
                return Fail("两次输入的密码不同");

            var q = UsersInfoService.UpdataPassWorrd(new UsersInfoSys { UserID = Operator.GetUserInfo().UserID, Password = pass });
            if (q == 1)
            {
                Operator.updataPass(pass);
            }
          return Json(q,JsonRequestBehavior.AllowGet);
        }
    }
}
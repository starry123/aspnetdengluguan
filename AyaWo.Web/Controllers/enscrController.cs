﻿using AyaWo.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AyaWo.Util;

namespace AyaWo.Web.Controllers
{
    public class enscrController : MvcControllerBase
    {
        // GET: enscr
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult upDateENCR()
        {
            string userID = Operator.GetUserInfo().UserID;
            string enscr = UsersInfoService.upDateENCR(userID);
            if (enscr.NotNullOrWhiteSpace())
                Operator.updataenscr(enscr);
            return Success(enscr);
        }
    }
}
﻿using AyaWo.Business;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AyaWo.Web.Controllers
{
    public class HeadImgController : MvcControllerBase
    {
        // GET: HeadImg
        public ActionResult Index()
        {
            var userID = Operator.GetUserInfo().UserID;
            var user = UsersInfoService.GetUserInfo(userID);

            ViewData["HeadIcon"] = user.HeadIcon;
            return View();
        }

        public ActionResult HeadImg(HttpPostedFileBase file)
        {
            var userID = Operator.GetUserInfo().UserID;
            if (file == null)
                return Json(new { result = "failed", message = "文件为空..." });
            string repath = "FilterUpload/imgUpload/" + userID + "/" + DateTime.Now.ToString("yyyyMMddHHmmss") + "/";
            //HttpPostedFileWrapper file
            string fpath = Server.MapPath("~/") + repath;
            string folder = Path.GetDirectoryName(fpath);
            if (!Directory.Exists(folder))
            {
                Directory.CreateDirectory(folder);
            }
            string name = file.FileName;//得到文件名
            folder = folder + "/" + name;
            repath = "/" + repath + "/" + name;
            file.SaveAs(folder);
            ImgH(repath);
            return Json(new { result = "ok", id = name, url = repath });
        }

        public void ImgH(string str)
        {
            var userID = Operator.GetUserInfo().UserID;
            UsersInfoSys entity = new UsersInfoSys {  UserID=userID,HeadIcon=str};
            var q = UsersInfoService.UpdateHeadIco(entity);
            if (q)
            {
                Operator.HeadIco(str);
            }
        }
    }
}
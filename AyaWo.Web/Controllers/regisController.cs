﻿using AyaWo.Business;
using AyaWo.Util;
using AyaWo.Util.Cache;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AyaWo.Web.Controllers
{
    [HandlerLogin(FilterMode.Ignore)]
    public class regisController : MvcControllerBase
    {
        // GET: regis
        public ActionResult Index()
        {
            //aya58 @foxmail.com
            return View();
        }

        [HttpPost]
        [AjaxOnly]
        [HandlerValidateAntiForgeryToken]
        public ActionResult SenEmil(string email)
        {
            int ecode = new Random().Next(1000, 9999);
            var bol = MailHelper.Send(email, "汀江网用户注册", "汀江网用户注册验证码：【" + ecode + " 】5 分钟内有效");
            if (bol){
                CacheFactory.Cache().Write<string>(email, ecode + "",DateTime.Now.AddMinutes(5));
                return Json(new { start = 1, msg = "验证码发送成功" });
            }
            else{
                return Json(new { start = 0, msg = "验证码发送失败，请检查邮箱或者联系管理员" });
            }
        }
        [HttpPost]
        [AjaxOnly]
        [HandlerValidateAntiForgeryToken]
        public ActionResult AddNewUser(string userName,string password,string emila,string vecod)
        {
            if (userName.IsNullOrWhiteSpace())
            {
                return Fail("用户账号不能为空...");
            }
            if (password.IsNullOrWhiteSpace())
            {
                return Fail("密码不能为空...");
            }
            if (emila.IsNullOrWhiteSpace())
            {
                return Fail("邮箱不能为空...");
            }
            if (vecod.IsNullOrWhiteSpace())
            {
                return Fail("验证码不能为空...");
            }

          var vox=  CacheFactory.Cache().Read<string>(emila);
            if (vox==null||!vox.Equals(vecod)) {
                return Fail("验证码不正确");
            }
            var q = UsersInfoService.AddUserInfo(new UsersInfoSys { Account=userName, Email=emila, Password=password, DeleteMark=0, emillValid=1, HeadIcon= "/Content/login/log.png",EnabledMark=1, Gender=0 });
            if (q){
                return Success("用户注册成功");
            }
            else {
                return Fail("用户注册失败");
            }
        }
    }
}
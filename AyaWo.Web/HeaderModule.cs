﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AyaWo.Web
{
    public class HeaderModule : IHttpModule
    {
        public void Init(HttpApplication context)
        {
            context.PreSendRequestHeaders += OnPreSendRequestHeaders;
        }

        public void Dispose() { }

        void OnPreSendRequestHeaders(object sender, EventArgs e)
        {
            //HttpContext.Current.Response.Headers.Remove("Server");
            // 你可以在此设置
            HttpContext.Current.Response.Headers.Set("Server", "CERN ayawo.com");
        }
    }
}
﻿
    var ayawo = new Object();
    ayawo.getData = function (form) {
        var result = {};
        $(form).find("[data-field]").each(function () {
            var field = $(this).attr("data-field"); var val;
            if ($(this).attr('type') == 'checkbox') {
                val = $(this).prop('checked');
            }
            else if ($(this).attr('type') == 'radio') {
                val = $(this).prop('checked');
            } else {
                val = $(this).val();
            }
            // 获取单个属性的值,并扩展到result对象里面         
            ayawo.getField(field.split('.'), val, result);
        }); return result;
    }

    ayawo.getField = function (fieldNames, value, result) {
        if (fieldNames.length > 1) {
            for (var i = 0; i < fieldNames.length - 1; i++) {
                if (result[fieldNames[i]] == undefined) {
                    result[fieldNames[i]] = {}
                }
                result = result[fieldNames[i]];
            }
            result[fieldNames[fieldNames.length - 1]] = value;
        }
        else { result[fieldNames[0]] = value; }
    }

    ayawo.setData = function (form, entity) {
        $(form).find("[data-field]").each(function () {
            var field = $(this).attr("data-field");
            fieldNames = field.split('.');
            var value = JSON.parse(JSON.stringify(entity));
            for (var index = 0; index < fieldNames.length; index++) {
                value = value[fieldNames[index]];
                if (!value) {
                    break;
                }
            }
            if ($(this).attr("type") === "checkbox" || $(this).attr("type") === "radio") {
                $(this).attr('checked', Boolean(value));
            }
            else {
                if (value) {
                    $(this).val(value);
                }
                else {
                    $(this).val("");
                }
            }
        })
    }


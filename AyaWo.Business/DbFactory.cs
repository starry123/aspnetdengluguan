﻿
using NPoco;
using System;
using Unit;

namespace AyaWo.DB
{
    /// <summary>
    /// 数据库服务工厂
    /// </summary>
    public class ctx 
    {
        #region MyRegion
        /// <summary>
        /// 数据库类型
        /// </summary>
        public static string dbType { get; set; }
        /// <summary>
        /// 数据连接
        /// </summary>
        public static string dbConnectionString { get; set; }

        /// <summary>
        /// 获取指定的数据库连接
        /// </summary>
        /// <returns></returns>
        public static IDatabase Init()
        {
            //数据库连接类型    /*"server=122.112.229.171;user id=ayawotinj ;password=ayawotinjdev;database=ayawotinj;charset=utf8",*/

            if (string.IsNullOrEmpty(dbType))
            {
                var list = DatHelper<Db>.GetDatList("Db.dat");
                var connString = Util.DESEncrypt.Decrypt(list[0].DbConnectionString.Trim(), "123567");
                dbType = list[0].DbType.Trim();
                dbConnectionString = connString;
            }
            DatabaseType sqlSourceType = DatabaseType.SQLite;
            switch (dbType)
            {
                case "SqlServer":
                    sqlSourceType = DatabaseType.SqlServer2012;
                    break;
                case "SqLite":
                    sqlSourceType = DatabaseType.SQLite;
                    break;
                case "MySql":
                    sqlSourceType = DatabaseType.MySQL;
                    break;
                case "Oracle":
                    sqlSourceType = DatabaseType.Oracle;
                    break;
                case "PostgreSQL":
                    sqlSourceType = DatabaseType.PostgreSQL;
                    break;
            }
            return new Database(dbConnectionString, sqlSourceType,MySql.Data.MySqlClient.MySqlClientFactory.Instance);
        }

        /// <summary>
        /// 获取指定的数据库连接
        /// </summary>
        /// <returns></returns>
        public static IDatabase Init(string dbType, string connectionString)
        {
            DatabaseType sqlSourceType = DatabaseType.SQLite;
            switch (dbType)
            {
                case "SqlServer":
                    sqlSourceType = DatabaseType.SqlServer2012;
                    break;
                case "SqLite":
                    sqlSourceType = DatabaseType.SQLite;
                    break;
                case "MySql":
                    sqlSourceType = DatabaseType.MySQL;
                    break;
                case "Oracle":
                    sqlSourceType = DatabaseType.Oracle;
                    break;
                case "PostgreSQL":
                    sqlSourceType = DatabaseType.PostgreSQL;
                    break;
                case "SQLCe":
                    sqlSourceType = DatabaseType.SQLCe;
                    break;
                case "OracleManaged":
                    sqlSourceType = DatabaseType.OracleManaged;
                    break;
            }
            return new Database(connectionString, sqlSourceType);
        }
        #endregion

    }

    public class Db
    {
        public string DbConnectionString { get; set; }
        public string DbType { get; set; }
    }

}

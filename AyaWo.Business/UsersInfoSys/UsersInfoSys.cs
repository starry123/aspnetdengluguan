﻿using AyaWo.Util;
using AyaWo.Util.Cache;
using NPoco;

namespace AyaWo.Business
{
    /// <summary>
    /// 版 本 AyaWo-ADMS 1.6.0
    /// 创 建：超级管理员
    /// 日 期：2018-07-09 14:48
    /// </summary>
    [TableName("usersinfosys")]
    [PrimaryKey("UserID")]
    public class UsersInfoSys
    {
        #region 用户信息

        /// <summary>
        /// 用户主键
        /// </summary>		
        public string UserID { get; set; }

        /// <summary>
        /// 账户
        /// </summary>	
        public string Account { get; set; }

        /// <summary>
        /// 登录密码
        /// </summary>		
        public string Password { get; set; }

        /// <summary>
        /// 真实姓名
        /// </summary>
        public string RealName { get; set; }

        /// <summary>
        /// 呢称
        /// </summary>	
        public string NickName { get; set; }

        /// <summary>
        /// 头像
        /// </summary>	
        public string HeadIcon { get; set; }

        /// <summary>
        /// 性别
        /// </summary>	
        public int? Gender { get; set; }

        /// <summary>
        /// 手机
        /// </summary>	
        public string Mobile { get; set; }

        /// <summary>
        /// 电话
        /// </summary>		
        public string Telephone { get; set; }

        /// <summary>
        /// 电子邮件
        /// </summary>	
        public string Email { get; set; }

        /// <summary>
        /// QQ号
        /// </summary>		
        public string OICQ { get; set; }

        /// <summary>
        /// 微信号
        /// </summary>	
        public string WeChat { get; set; }


        public int DeleteMark { get; set; }

        public int EnabledMark { get; set; }

        /// <summary>
        /// 手机号码校验
        /// </summary>
        public int? phonValid { get; set; }

        /// <summary>
        /// 邮箱校验
        /// </summary>
        public int? emillValid { get; set; }

        /// <summary>
        /// qq校验
        /// </summary>
        public int? oicqValid { get; set; }

        /// <summary>
        /// 单点登录标识
        /// </summary>		
        [Ignore]
        public int? OpenId { get; set; }

        #endregion

        [Ignore]
        public string LoginMsg { get; set; }

        [Ignore]
        public bool LoginOk { get; set; }
    }

    public class Operator
    {
        public static UsersInfoSys GetUserInfo()
        {
            UsersInfoSys usersInfo = new UsersInfoSys();
            var guid = WebHelper.GetCookie("ayawo_value");
            if (guid.IsNullOrWhiteSpace())
            {
                usersInfo.LoginMsg = "缓存已经过期";
                return usersInfo;
            }
            var q = CacheFactory.Cache().Read<UsersInfoSys>(guid);
            if (q == null)
            {
                usersInfo.LoginMsg = "缓存已经过期";
                return usersInfo;
            }
            else
                return q;
        }

        public static void OutLog()
        {
            var guid = WebHelper.GetCookie("ayawo_value");
            if (guid.NotNullOrWhiteSpace())
            {
                WebHelper.RemoveCookie("ayawo_value");
            }
            var q = CacheFactory.Cache().Read<UsersInfoSys>(guid);
            if (q != null)
                CacheFactory.Cache().Remove(guid);
        }

        public static void updataPass(string pass)
        {
            var guid = WebHelper.GetCookie("ayawo_value");
            var q = CacheFactory.Cache().Read<UsersInfoSys>(guid);
            if (q != null)
            {
                q.Password = pass;
                CacheFactory.Cache().Write<UsersInfoSys>(guid,q);
            }
        }

        public static void HeadIco(string icoImg)
        {
            var guid = WebHelper.GetCookie("ayawo_value");
            var q = CacheFactory.Cache().Read<UsersInfoSys>(guid);
            if (q != null)
            {
                q.HeadIcon = icoImg;
                CacheFactory.Cache().Write<UsersInfoSys>(guid, q);
            }
        }
    }
}



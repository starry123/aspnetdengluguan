﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AyaWo.DB;
using AyaWo.Util;

namespace AyaWo.Business
{
    public class UsersInfoService
    {
        public static UsersInfoSys CheckLogin(string username, string password)
        {
            try
            {
                UsersInfoSys usersInfoSys = new UsersInfoSys();

                if (username.IsNullOrWhiteSpace())
                {
                    usersInfoSys.LoginMsg = "用户名为空....";
                    return usersInfoSys;
                }
                if (password.IsNullOrWhiteSpace())
                {
                    usersInfoSys.LoginMsg = "密码为空....";
                    return usersInfoSys;
                }

                usersInfoSys = ctx.Init().Single<UsersInfoSys>("select * from UsersInfoSys where Account=@0 and Password=@1 ", username, password);

                if (usersInfoSys != null)
                {
                    usersInfoSys.LoginOk = true;
                }
                else
                {
                    usersInfoSys.LoginOk = false;
                    usersInfoSys.LoginMsg = "用户名或者密码错误";
                }
                return usersInfoSys;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public static int UpdataPassWorrd(UsersInfoSys obj)
        {
            try
            {
                //var ty = ctx.Init().Get<UsersInfoSys>(obj.UserID);
                //ty.Password = obj.Password;

                var q = ctx.Init().Execute("UPDATE usersinfosys SET `PassWord`=@PassWord WHERE UserID=@UserID", new { UserID = obj.UserID, Password = obj.Password });
                return q;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取用户信息
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        public static UsersInfoSys GetUserInfo(string userID)
        {
            try
            {
                return ctx.Init().First<UsersInfoSys>("WHERE UserID=@UserID", new { UserID = userID });
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 判断账户名称的唯一性
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="account"></param>
        /// <returns></returns>
        public static bool AccountOnly(string userID, string account)
        {
            try
            {
                var q = ctx.Init().Fetch<UsersInfoSys>("WHERE UserID!=@UserID AND Account=@Account", new { UserID = userID, Account = account });
                if (q == null || q.Count() == 0)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 判断昵称的唯一性
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="account"></param>
        /// <returns></returns>
        public static bool NickNameOnly(string userID, string nickName)
        {
            try
            {
                var q = ctx.Init().Fetch<UsersInfoSys>("WHERE UserID!=@UserID AND NickName=@NickName", new { UserID = userID, NickName = nickName });
                if (q == null || q.Count() == 0)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public static bool SaveEntity(UsersInfoSys obj)
        {
            try
            {
                var o = ctx.Init().SingleById<UsersInfoSys>((object)obj.UserID);
                if (o != null)
                {
                    o.Account = obj.Account;
                    o.NickName = obj.NickName;
                    o.RealName = obj.RealName;
                    o.OICQ = obj.OICQ;
                    o.Gender = obj.Gender;
                    o.Telephone = obj.Telephone;
                    o.Email = obj.Email;
                    o.WeChat = obj.WeChat;
                    return ctx.Init().Update(o) == 1 ? true : false;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static bool UpdateHeadIco(UsersInfoSys obj)
        {
            try
            {
                if (obj.UserID.IsNullOrWhiteSpace())
                    return false;
                var o = ctx.Init().SingleById<UsersInfoSys>((object)obj.UserID);
                if (o != null)
                {
                    o.HeadIcon = obj.HeadIcon;
                    return ctx.Init().Update(o) == 1 ? true : false;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }


       /// <summary>
       /// 新增注册用户
       /// </summary>
       /// <param name="obj"></param>
       /// <returns></returns>
        public static bool AddUserInfo(UsersInfoSys obj)
        {
            try {
                obj.UserID = OrderHelper.GenerateId("U");

           string sqlstr="INSERT usersinfosys(UserID,Account, Email,`Password`, DeleteMark, emillValid, HeadIcon, EnabledMark, Gender) VALUES(@UserID,@Account, @Email, @Password, 0, 1, '/Content/login/log.png', 1, 0)";

               return ctx.Init().Execute(sqlstr,new { UserID=obj.UserID, Account = obj.Account, Email = obj.Email, Password = obj.Password, DeleteMark = 0, emillValid = 1, HeadIcon = "/Content/login/log.png", EnabledMark = 1, Gender = 0 }) ==0?false:true;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
    }
}

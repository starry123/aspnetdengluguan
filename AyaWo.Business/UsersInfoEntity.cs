﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AyaWo.Entity
{
    /// <summary>
    /// 版 本 AyaWo-ADMS 1.6.0
    /// 创 建：超级管理员
    /// 日 期：2018-07-09 14:48
    /// </summary>
    public class UsersInfoSys
    {
        #region 用户信息

        /// <summary>
        /// 用户主键
        /// </summary>		
        [Key]
        public string UserID { get; set; }
        /// <summary>
        /// 工号
        /// </summary>	
        public string EnCode { get; set; }
        /// <summary>
        /// 账户
        /// </summary>	
        public string Account { get; set; }
        /// <summary>
        /// 登录密码
        /// </summary>		
        public string Password { get; set; }
        /// <summary>
        /// 密码秘钥
        /// </summary>	
        public string Secretkey { get; set; }
        /// <summary>
        /// 真实姓名
        /// </summary>
        public string RealName { get; set; }
        /// <summary>
        /// 呢称
        /// </summary>	
        public string NickName { get; set; }
        /// <summary>
        /// 头像
        /// </summary>	
        public string HeadIcon { get; set; }
        /// <summary>
        /// 性别
        /// </summary>	
        public int? Gender { get; set; }
        /// <summary>
        /// 手机
        /// </summary>	
        public string Mobile { get; set; }
        /// <summary>
        /// 电话
        /// </summary>		
        public string Telephone { get; set; }
        /// <summary>
        /// 电子邮件
        /// </summary>	
        public string Email { get; set; }
        /// <summary>
        /// QQ号
        /// </summary>		
        public string OICQ { get; set; }
        /// <summary>
        /// 微信号
        /// </summary>	
        public string WeChat { get; set; }
        /// <summary>
        /// 是否是超级管理员
        /// </summary>
        public bool IsSystem { get; set; }

        public int DeleteMark { get; set; }
        
        /// <summary>
        /// 公司主键
        /// </summary>		
        [NotMapped]
        public string CompanyId { get; set; }
        /// <summary>
        /// 所在公司及下属公司
        /// </summary>
        [NotMapped]
        public List<string> CompanyIds { get; set; }
        /// <summary>
        /// 部门主键
        /// </summary>		
        [NotMapped]
        public string DepartmentId { get; set; }
        /// <summary>
        /// 所在部门及下属部门
        /// </summary>
        [NotMapped]
        public List<string> DepartmentIds { get; set; }
        /// <summary>
        /// 单点登录标识
        /// </summary>		
        [NotMapped]
        public int? OpenId { get; set; }
        /// <summary>
        /// 角色信息
        /// </summary>
        [NotMapped]
        public string RoleIds { get; set; }
        /// <summary>
        /// 岗位信息
        /// </summary>
        [NotMapped]
        public string PostIds { get; set; }
       
        #endregion

        #region 扩展信息
        /// <summary>
        /// 应用Id
        /// </summary>
        [NotMapped]
        public string appId { get; set; }
        /// <summary>
        /// 登录时间
        /// </summary>
        [NotMapped]
        public DateTime logTime { get; set; }
        /// <summary>
        /// 登录IP地址
        /// </summary>
        [NotMapped]
        public string iPAddress { get; set; }
        /// <summary>
        /// 浏览器名称
        /// </summary>
        [NotMapped]
        public string browser { get; set; }
        /// <summary>
        /// 登录者标识
        /// </summary>
        [NotMapped]
        public string loginMark { get; set; }
        /// <summary>
        /// 票据信息
        /// </summary>
        [NotMapped]
        public string token { get; set; }
        /// <summary>
        /// 即时通讯地址
        /// </summary>
        [NotMapped]
        public string imUrl { get; set; }
        #endregion
        [NotMapped]
        public string LoginMsg { get; set; }

        [NotMapped]
        public bool LoginOk { get; set; }
    
        public int EnabledMark { get; set; }
        
    }
}



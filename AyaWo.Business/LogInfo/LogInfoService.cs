﻿using AyaWo.DB;
using AyaWo.Util;
using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AyaWo.Business
{
    public static class LogInfoService
    {
        public static void WriteLog(this LogInfo logInfo)
        {
            try
            {
                logInfo.LogId = Guid.NewGuid().ToString();
                logInfo.EnabledMark = 1;
                logInfo.OperateTime = DateTime.Now;
                logInfo.DeleteMark = 0;
                var sqt = new StringBuilder(@"insert into loginfosys(LogId ,CategoryId,SourceObjectId,SourceContentJson,OperateTime,OperateUserId,OperateAccount,OperateTypeId,OperateType,Module,IPAddress,IPAddressName,Host,Browser,ExecuteResult,ExecuteResultJson,Description,DeleteMark,EnabledMark )
                                                             values(@LogId,@CategoryId,@SourceObjectId,@SourceContentJson,@OperateTime,@OperateUserId,@OperateAccount,@OperateTypeId,@OperateType,@Module,@IPAddress,@IPAddressName,@Host,@Browser,@ExecuteResult,@ExecuteResultJson,@Description,@DeleteMark,@EnabledMark)");
                ctx.Init().Execute(sqt.ToString(), logInfo);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
    }

    public class LogInfoServices
    {
        public static Page<LogInfo> LoginLog(string OperateUserId, int page = 1, int pageSize = 12)
        {
            var q = ctx.Init().Page<LogInfo>(page, pageSize, "SELECT OperateTime, OperateType, IPAddress, Browser, ExecuteResultJson FROM loginfosys WHERE OperateUserId = @0 ORDER BY OperateTime desc", OperateUserId);
            return q;
        }
    }
}

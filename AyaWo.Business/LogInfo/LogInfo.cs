﻿using NPoco;
using System;

namespace AyaWo.Business
{
    /// 创建人：starry
    /// 日 期：2018.07.09
    /// 描 述：系统日志
    /// </summary>
    [TableName("loginfosys")]
    [PrimaryKey("LogId")]
    public class LogInfo
    {
        #region 实体成员
        /// <summary>
        /// 日志主键
        /// </summary>
        /// <returns></returns>
        [Column("LogId")]
        public string LogId { get; set; }
        /// <summary>
        /// 分类Id 1-登陆2-访问3-操作4-异常
        /// </summary>
        /// <returns></returns>
        [Column("CategoryId")]
        public int? CategoryId { get; set; }
        /// <summary>
        /// 来源对象主键
        /// </summary>
        /// <returns></returns>
        [Column("SourceObjectId")]
        public string SourceObjectId { get; set; }
        /// <summary>
        /// 来源日志内容
        /// </summary>
        /// <returns></returns>
        [Column("SourceContentJson")]
        public string SourceContentJson { get; set; }
        /// <summary>
        /// 操作时间
        /// </summary>
        /// <returns></returns>
        [Column("OperateTime")]
        public DateTime? OperateTime { get; set; }
        /// <summary>
        /// 操作用户Id
        /// </summary>
        /// <returns></returns>
        [Column("OperateUserId")]
        public string OperateUserId { get; set; }
        /// <summary>
        /// 操作用户
        /// </summary>
        /// <returns></returns>
        [Column("OperateAccount")]
        public string OperateAccount { get; set; }
        /// <summary>
        /// 操作类型Id
        /// </summary>
        /// <returns></returns>
        [Column("OperateTypeId")]
        public string OperateTypeId { get; set; }
        /// <summary>
        /// 操作类型
        /// </summary>
        /// <returns></returns>
        [Column("OperateType")]
        public string OperateType { get; set; }
        /// <summary>
        /// 系统功能
        /// </summary>
        /// <returns></returns>
        [Column("Module")]
        public string Module { get; set; }
        /// <summary>
        /// IP地址
        /// </summary>
        /// <returns></returns>
        [Column("IPAddress")]
        public string IPAddress { get; set; }
        /// <summary>
        /// IP地址所在城市
        /// </summary>
        /// <returns></returns>
        [Column("IPAddressName")]
        public string IPAddressName { get; set; }
        /// <summary>
        /// 主机
        /// </summary>
        /// <returns></returns>
        [Column("Host")]
        public string Host { get; set; }
        /// <summary>
        /// 浏览器
        /// </summary>
        /// <returns></returns>
        [Column("Browser")]
        public string Browser { get; set; }
        /// <summary>
        /// 执行结果状态
        /// </summary>
        /// <returns></returns>
        [Column("ExecuteResult")]
        public int? ExecuteResult { get; set; }
        /// <summary>
        /// 执行结果信息
        /// </summary>
        /// <returns></returns>
        [Column("ExecuteResultJson")]
        public string ExecuteResultJson { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        /// <returns></returns>
        [Column("Description")]
        public string Description { get; set; }
        /// <summary>
        /// 删除标记
        /// </summary>
        /// <returns></returns>
        [Column("DeleteMark")]
        public int? DeleteMark { get; set; }
        /// <summary>
        /// 有效标志
        /// </summary>
        /// <returns></returns>
        [Column("EnabledMark")]
        public int? EnabledMark { get; set; }
        #endregion
    }
}

﻿ 

SET FOREIGN_KEY_CHECKS=0;
 
 
-- ----------------------------
-- Table structure for loginfo
-- ----------------------------
DROP TABLE IF EXISTS `loginfo`;
CREATE TABLE `loginfo` (
  `LogId` varchar(50) NOT NULL COMMENT '日志主键',
  `CategoryId` int(11) DEFAULT NULL COMMENT '分类Id',
  `SourceObjectId` varchar(50) DEFAULT NULL COMMENT '来源对象主键',
  `SourceContentJson` varchar(255) DEFAULT NULL COMMENT '来源日志内容',
  `OperateTime` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '操作时间',
  `OperateUserId` varchar(50) DEFAULT NULL COMMENT '操作用户Id',
  `OperateAccount` varchar(255) DEFAULT NULL COMMENT '操作用户',
  `OperateTypeId` varchar(50) DEFAULT NULL COMMENT '操作类型Id',
  `OperateType` varchar(255) DEFAULT NULL COMMENT '操作类型',
  `Module` varchar(255) DEFAULT NULL COMMENT '系统功能',
  `IPAddress` varchar(255) DEFAULT NULL COMMENT 'IP地址',
  `IPAddressName` varchar(255) DEFAULT NULL COMMENT 'IP地址所在城市',
  `Host` varchar(255) DEFAULT NULL COMMENT '主机',
  `Browser` varchar(255) DEFAULT NULL COMMENT '浏览器',
  `ExecuteResult` varchar(255) DEFAULT NULL COMMENT '执行结果状态',
  `ExecuteResultJson` varchar(255) DEFAULT NULL COMMENT '执行结果信息',
  `Description` varchar(255) DEFAULT NULL COMMENT '备注',
  `DeleteMark` int(11) DEFAULT NULL COMMENT '删除标记',
  `EnabledMark` int(11) DEFAULT NULL COMMENT '有效标志',
  PRIMARY KEY (`LogId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统日志';

-- ----------------------------
-- Records of loginfo
-- ----------------------------
INSERT INTO `loginfo` VALUES ('2530485e-d039-4601-adf6-24fd741fc477', '1', null, null, '2018-07-10 15:53:40', '1', 'admin(Admin)', '1', '登录', 'Chinaz', '127.0.0.1', null, 'DESKTOP-5TH661G', 'Firefox 61.0', '1', '登录成功', null, '0', '1');
 

-- ----------------------------
-- Table structure for usersinfo
-- ----------------------------
DROP TABLE IF EXISTS `usersinfo`;
CREATE TABLE `usersinfo` (
  `UserID` varchar(50) NOT NULL COMMENT '用户主键',
  `EnCode` varchar(50) DEFAULT NULL COMMENT '工号',
  `Account` varchar(50) DEFAULT NULL COMMENT '账户',
  `PassWord` varchar(50) DEFAULT NULL COMMENT '登录密码',
  `Secretkey` varchar(50) DEFAULT NULL COMMENT '密码秘钥',
  `RealName` varchar(50) DEFAULT NULL COMMENT '真实姓名',
  `NickName` varchar(50) DEFAULT NULL COMMENT '呢称',
  `HeadIcon` varchar(50) DEFAULT NULL COMMENT '头像',
  `Gender` int(1) DEFAULT NULL COMMENT '性别',
  `Mobile` varchar(20) DEFAULT NULL COMMENT '手机',
  `Telephone` varchar(20) DEFAULT NULL COMMENT '电话',
  `Email` varchar(20) DEFAULT NULL COMMENT '电子邮件',
  `OICQ` varchar(20) DEFAULT NULL COMMENT 'QQ号',
  `EeChat` varchar(20) DEFAULT NULL COMMENT '微信号',
  `IsSystem` int(1) DEFAULT NULL COMMENT '是否是超级管理员',
  `DeleteMark` int(1) DEFAULT NULL,
  `EnabledMark` int(1) DEFAULT NULL,
  PRIMARY KEY (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户表';

-- ----------------------------
-- Records of usersinfo
-- ----------------------------
INSERT INTO `usersinfo` VALUES ('1', 'admin', 'admin', '510386da7bb75198462edcfbae92c8c4', 'chinaz', 'Admin', null, null, null, null, null, null, null, null, null, '0', '1');

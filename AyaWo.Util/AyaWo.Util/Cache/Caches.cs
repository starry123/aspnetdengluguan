﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Caching;

namespace AyaWo.Util.Cache
{
    public class Caches :ICache
    {
        /// <summary>
        /// 读取缓存
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="cacheKey"></param>
        /// <param name="dbId"></param>
        /// <returns></returns>
        public   T Read<T>(string cacheKey, long dbId = 0) where T : class
        {
            var objCache = HttpRuntime.Cache.Get(cacheKey);
            return (T)objCache;
        }

        /// <summary>
        /// 移除指定数据缓存
        /// </summary>
        /// <param name="cacheKey"></param>
        /// <param name="dbId"></param>
        public   void Remove(string cacheKey, long dbId = 0)
        {
            var cache = HttpRuntime.Cache;
            cache.Remove(cacheKey);
        }

       /// <summary>
       /// 
       /// </summary>
       /// <param name="dbId"></param>
        public   void RemoveAll(long dbId = 0)
        {
            var cache = HttpRuntime.Cache;
            var cacheEnum = cache.GetEnumerator();
            while (cacheEnum.MoveNext())
            {
                cache.Remove(cacheEnum.Key.ToString());
            }
        }

        /// <summary>
        /// 写入缓存
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="cacheKey"></param>
        /// <param name="value"></param>
        /// <param name="dbId"></param>
        public   void Write<T>(string cacheKey, T value, long dbId = 0) where T : class
        {
            try
            {
                if (value == null) return;
                var objCache = HttpRuntime.Cache;
                objCache.Insert(cacheKey, value, null, DateTime.Now.AddSeconds(7200), TimeSpan.Zero, CacheItemPriority.High, null);
            }
            catch (Exception)
            {
                //throw;  
            }
        }

        public   void Write<T>(string cacheKey, T value, TimeSpan timeSpan, long dbId = 0) where T : class
        {
            try
            {
                if (value == null) return;
                var objCache = HttpRuntime.Cache;
                objCache.Insert(cacheKey, value, null, DateTime.Now.AddSeconds(timeSpan.Seconds), TimeSpan.Zero, CacheItemPriority.High, null);
            }
            catch (Exception)
            {
                //throw;  
            }
        }


        /// <summary>
        /// 写入缓存
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="cacheKey"></param>
        /// <param name="value"></param>
        /// <param name="expireTime"></param>
        /// <param name="dbId"></param>
        public   void Write<T>(string cacheKey, T value, DateTime expireTime, long dbId = 0) where T : class
        {
            try
            {
                if (value == null) return;
                var objCache = HttpRuntime.Cache;
                //相对过期  
                //objCache.Insert(cacheKey, objObject, null, DateTime.MaxValue, timeout, CacheItemPriority.NotRemovable, null);  
                //绝对过期时间  
                objCache.Insert(cacheKey, value, null, expireTime, TimeSpan.Zero, CacheItemPriority.High, null);
            }
            catch (Exception)
            {
                //throw;  
            }
        }
    }
 
}

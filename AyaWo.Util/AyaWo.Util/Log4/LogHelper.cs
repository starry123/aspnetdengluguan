﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

[assembly: log4net.Config.XmlConfigurator(Watch = true)]

namespace AyaWo.Util
{
    public class LogHelper : ILogHelper
    {
        #region private
        private static Dictionary<string, log4net.ILog> listILog = new Dictionary<string, log4net.ILog>();   //Log对象集合
        private log4net.ILog iLog;  //当前日志对象的实例
        #endregion
        /// <summary>
        /// 构造函数，传入Log4NET 的ILog对象
        /// </summary>
        /// <param name="logger"></param>
        public LogHelper(log4net.ILog logger)
        {
            string loggerName = logger.Logger.Name;     //logger 配置节名称
            if (!listILog.ContainsKey(loggerName))
            {
                lock (listILog)
                {
                    if (!listILog.ContainsKey(loggerName))
                    {
                        listILog.Add(loggerName, logger);
                    }
                    else
                    {
                        listILog[loggerName] = logger;
                    }
                }
            }
            else if (listILog[loggerName] == null)
            {
                listILog[loggerName] = logger;
            }
            iLog = listILog[loggerName];
        }

        public LogHelper(string loggerName)
        {
            log4net.ILog logger = log4net.LogManager.GetLogger(loggerName);
            string LoggerName = logger.Logger.Name;     //logger 配置节名称
            if (!listILog.ContainsKey(LoggerName))
            {
                listILog.Add(LoggerName, logger);
            }
            else if (listILog[LoggerName] == null)
            {
                listILog[LoggerName] = logger;
            }
            iLog = listILog[LoggerName];
        }
      



        public void Debug(string message, Exception exception = null)
        {
            if (iLog.IsDebugEnabled)
            {
                if (exception != null)
                {
                    iLog.Debug(message, exception);
                }
                else
                {
                    iLog.Debug(message);
                }
            }
        }

        public void Error(string message, Exception exception = null)
        {
            if (iLog.IsErrorEnabled)
            {
                if (exception != null)
                {
                    iLog.Debug(message, exception);
                }
                else
                {
                    iLog.Debug(message);
                }
            }
        }

        public void Fatal(string message, Exception exception = null)
        {
            if (iLog.IsFatalEnabled)
            {
                if (exception != null)
                {
                    iLog.Fatal(message, exception);
                }
                else
                {
                    iLog.Fatal(message);
                }
            }
        }

        public void Info(string message, Exception exception = null)
        {
            if (iLog.IsInfoEnabled)
            {
                if (exception != null)
                {
                    iLog.Info(message, exception);
                }
                else
                {
                    iLog.Info(message);
                }
            }
        }

        public void Warn(string message, Exception exception = null)
        {
            if (iLog.IsWarnEnabled)
            {
                if (exception != null)
                {
                    iLog.Warn(message, exception);
                }
                else
                {
                    iLog.Warn(message);
                }
            }
        }
    }
}

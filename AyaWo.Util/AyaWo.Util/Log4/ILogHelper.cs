﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AyaWo.Util
{
    public interface ILogHelper
    {
        /// <summary>
        /// 写入Debug日志,
        /// </summary>
        /// <param name="message">日志信息,占位符为 %Object{Message}</param>
        void Debug(string message, Exception exception = null);

        /// <summary>
        /// 写入Info日志,
        /// </summary>
        /// <param name="message">日志信息,占位符为 %Object{Message}</param>
        void Info(string message, Exception exception = null);

        /// <summary>
        /// 写入Warn日志,
        /// </summary>
        /// <param name="message">日志信息,占位符为 %Object{Message}</param>
        void Warn(string message, Exception exception = null);


        /// <summary>
        /// 写入Error日志,
        /// </summary>
        /// <param name="message">日志信息,占位符为 %Object{Message}</param>
        void Error(string message, Exception exception = null);


        /// <summary>
        /// 写入Fatal日志,
        /// </summary>
        /// <param name="message">日志信息,占位符为 %Object{Message}</param>
        void Fatal(string message, Exception exception = null);
    }
}

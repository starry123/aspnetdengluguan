﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using log4net;
using log4net.Config;

namespace AyaWo.Util
{
    public class LogManager
    {
       /* /// <summary>
        /// 初始化HY.Log,  Log配置文件需要写到 Web.config  OR  App.config
        /// </summary>
        public static void Init()
        {
            XmlConfigurator.Configure();
        }

        /// <summary>
        /// 初始化HY.Log, 
        /// </summary>
        /// <param name="configFileName">制定Log配置文件的文件绝对路径</param>
        public static void Init(string configFileName)
        {
            XmlConfigurator.Configure(new FileInfo(configFileName));
        }
*/


        /// <summary>
        /// 检索Logger名称返回日志处理接口
        /// </summary>
        /// <param name="name">Logger名称</param>
        /// <returns>日志接口</returns>
        public static LogHelper GetLogger(string name)
        {
            var log4Logger = log4net.LogManager.GetLogger(name);
            return new LogHelper(log4Logger);
        }

        /// <summary>
        /// 索Logger名称返回日志处理接口
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        public static LogHelper GetLogger(Type t)
        {
           
            var log4Logger = log4net.LogManager.GetLogger(t);
            return new LogHelper(log4Logger);
        }

        /// <summary>
        /// 检索Logger名称返回日志处理接口
        /// </summary>
        /// <param name="name">Logger名称</param>
        /// <returns>日志接口</returns>
        public static LogHelper GetLogger()
        {
            var log4Logger = log4net.LogManager.GetLogger("错误信息：");
            return new LogHelper(log4Logger);
        }
    }
}

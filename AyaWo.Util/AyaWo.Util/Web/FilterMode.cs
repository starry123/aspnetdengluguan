﻿namespace AyaWo.Util
{
    /// <summary>
    /// 版 本 V.5.0.3
    /// Copyright (c) 2013-2017 
    /// 创建人：starryLuo
    /// 日 期：2017.03.07
    /// 描 述：mvc过滤模式
    /// </summary>
    public enum FilterMode
    {
        /// <summary>执行</summary>
        Enforce,
        /// <summary>忽略</summary>
        Ignore
    }
}

﻿namespace AyaWo.Util
{
    /// <summary>
    /// 版 本 V.5.0.3
    /// Copyright (c) 2013-2017 
    /// 创建人：starryLuo
    /// 日 期：2017.03.08
    /// 描 述：接口响应数据
    /// </summary>
    public class ResParameter
    {
        /// <summary>
        /// 接口响应码
        /// </summary>
        public ResponseCode code { get; set; }
        /// <summary>
        /// 接口响应消息
        /// </summary>
        public string info { get; set; }
        /// <summary>
        /// 接口响应数据
        /// </summary>
        public object data { get; set; }
    }
}

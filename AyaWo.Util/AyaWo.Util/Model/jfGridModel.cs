﻿namespace AyaWo.Util
{
    /// <summary>
    /// 版 本 V.5.0.3
    /// Copyright (c) 2013-2017 
    /// 创建人：starryLuo
    /// 日 期：2017.07.10
    /// 描 述：表格属性模型
    /// </summary>
    public class jfGridModel
    {
        public string name { get; set; }
        public string label { get; set; }
        public string width { get; set; }
        public string align { get; set; }
        public string height { get; set; }
        public string hidden { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Dynamic;

namespace AyaWo.Util
{
    public static class myValidate
    {

        public static ValidateObject IsNull(this string value)
        {
            var ex = new ValidateObject();
            if (value.IsNullOrWhiteSpace())
            {
                ex.Success = false;
                ex.Result = "不能为空...";
            }
            else
            {
                ex.Success = true;
            }
            return ex;
        }

        /// <summary>
        /// 判断浮点型  //只能包含数字、小数点等字符
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static ValidateObject IsFloat(this string value)
        {
            Regex isFloat = new Regex(@"^[-\+]?\d+(\.\d +)?$");
            var ex = new ValidateObject();
            var m = isFloat.Match(value);
            ex.Success = m.Success;
            ex.Result = " 只能包含数字、小数点等字符";
            return ex;
        }
        public static ValidateObject IsFloats(this string value)
        {
            if (value.IsNullOrWhiteSpace())
                return new ValidateObject { Success = true };
            else
            {
                return IsFloat(value);
            }
        }

        ////判断中文字符  中文字符
        public static ValidateObject IsChinese(this string value)
        {
            Regex isChinese = new Regex(@"^[\u0391-\uFFE5]+$");
            var ex = new ValidateObject();
            var m = isChinese.Match(value);
            ex.Success = m.Success;
            ex.Result = " 只能中文字符";
            return ex;
        }
        public static ValidateObject IsChineses(this string value)
        {
            if (value.IsNullOrWhiteSpace())
                return new ValidateObject { Success = true };
            else
            {
                return IsChinese(value);
            }
        }

        ///// <summary>
        ///// 判断英文字符    只能包含英文字符。
        ///// </summary>
        public static ValidateObject IsEnglish(this string value)
        {
            Regex isEnglish = new Regex(@"^[A-Za-z]+$");
            var ex = new ValidateObject();
            var m = isEnglish.Match(value);
            ex.Success = m.Success;
            ex.Result = " 只能包含英文字符";
            return ex;
        }
        public static ValidateObject IsEnglishs(this string value)
        {
            if (value.IsNullOrWhiteSpace())
                return new ValidateObject { Success = true };
            else
            {
                return IsEnglish(value);
            }
        }

        ///// <summary>
        ///// 手机号码验证  请正确填写您的手机号码。
        ///// </summary>
        public static ValidateObject IsMobile(this string value)
        {
            Regex isMobile = new Regex(@"^(((13[0-9]{1})|(15[0-9]{1})|(18[0-9]{ 1}))+\d{ 8})$");
            var ex = new ValidateObject();
            var m = isMobile.Match(value);
            ex.Success = m.Success;
            ex.Result = " 请正确填写您的手机号码";
            return ex;
        }
        public static ValidateObject IsMobiles(this string value)
        {
            if (value.IsNullOrWhiteSpace())
                return new ValidateObject { Success = true };
            else
            {
                return IsMobile(value);
            }
        }

        ///// <summary>
        ///// 电话号码验证   请正确填写您的电话号码。
        ///// </summary>
        public static ValidateObject IsPhone(this string value)
        {
            Regex isPhone = new Regex(@"^(\d{3,4}-?)?\d{ 7,9}$");
            var ex = new ValidateObject();
            var m = isPhone.Match(value);
            ex.Success = m.Success;
            ex.Result = " 请正确填写您的电话号码";
            return ex;
        }
        public static ValidateObject IsPhones(this string value)
        {
            if (value.IsNullOrWhiteSpace())
                return new ValidateObject { Success = true };
            else
            {
                return IsPhone(value);
            }
        }

        ///// <summary>
        ///// 联系电话(手机/电话皆可)验证    请正确填写您的联系方式
        ///// </summary>
        public static ValidateObject IsTel(this string value)
        {
            Regex isTel = new Regex(@"^(\(\d{3,4}\)|\d{3,4}-)?\d{7,8}$|^(13|15)\d{9}$");
            var ex = new ValidateObject();
            var m = isTel.Match(value);
            ex.Success = m.Success;
            ex.Result = " 请正确填写您的联系方式";
            return ex;
        }
        public static ValidateObject IsTels(this string value)
        {
            if (value.IsNullOrWhiteSpace())
                return new ValidateObject { Success = true };
            else
            {
                return IsTel(value);
            }
        }

        ////匹配qq   匹配QQ
        public static ValidateObject IsQq(this string value)
        {
            Regex isQq = new Regex(@"^[1-9]\d{4,12}$");
            var ex = new ValidateObject();
            var m = isQq.Match(value);
            ex.Success = m.Success;
            ex.Result = "请输入正确的QQ";
            return ex;
        }
        public static ValidateObject IsQqs(this string value)
        {
            if (value.IsNullOrWhiteSpace())
                return new ValidateObject { Success = true };
            else
            {
                return IsQq(value);
            }
        }

        ///// <summary>
        ///// 匹配密码    以字母开头，长度在6-12之间，只能包含字符、数字和下划线。
        ///// </summary>
        public static ValidateObject IsPwd(this string value)
        {
            Regex isPwd = new Regex(@"^[a-zA-Z]\\w{6,12}$");
            var ex = new ValidateObject();
            var m = isPwd.Match(value);
            ex.Success = m.Success;
            ex.Result = "请输入以字母开头，长度在6-12之间，只能包含字符、数字和下划线。";
            return ex;
        }
        public static ValidateObject IsPwds(this string value)
        {
            if (value.IsNullOrWhiteSpace())
                return new ValidateObject { Success = true };
            else
            {
                return IsPwd(value);
            }
        }

        ///// <summary>
        ///// 身份证号码验证  请输入正确的身份证号码。
        ///// </summary>
        public static ValidateObject IsIdCardNo(this string value)
        {
            Regex isIdCardNo = new Regex(@"^(\d{6})()?(\d{4})(\d{2})(\d{2})(\d{3})(\w)$");
            var ex = new ValidateObject();
            var m = isIdCardNo.Match(value);
            ex.Success = m.Success;
            ex.Result = "请输入正确的身份证号码";
            return ex;
        }
        public static ValidateObject IsIdCardNos(this string value)
        {
            if (value.IsNullOrWhiteSpace())
                return new ValidateObject { Success = true };
            else
            {
                return IsIdCardNo(value);
            }
        }

        ///// <summary>
        ///// IP地址验证  请填写正确的IP地址。
        ///// </summary>
        public static ValidateObject IsIP(this string value)
        {
            Regex ip = new Regex(@"^(([1-9]|([1-9]\d)|(1\d\d)|(2([0-4]\d|5[0-5])))\.)(([1-9]|([1-9]\d)|(1\d\d)|(2([0-4]\d|5[0-5])))\.){2}([1-9]|([1-9]\d)|(1\d\d)|(2([0-4]\d|5[0-5])))$");
            var ex = new ValidateObject();
            var m = ip.Match(value);
            ex.Success = m.Success;
            ex.Result = "请输入正确的IP地址";
            return ex;
        }
        public static ValidateObject IsIPs(this string value)
        {
            if (value.IsNullOrWhiteSpace())
                return new ValidateObject { Success = true };
            else
            {
                return IsIP(value);
            }
        }

        ///// <summary>
        ///// 字符验证   只能包含中文、英文、数字、下划线等字符。
        ///// </summary>
        public static ValidateObject StringCheck(this string value)
        {
            Regex stringCheck = new Regex("^[a-zA-Z0-9\u4e00-\u9fa5-_]+$");
            var ex = new ValidateObject();
            var m = stringCheck.Match(value);
            ex.Success = m.Success;
            ex.Result = "请输入中文、英文、数字、下划线等字符";
            return ex;
        }
        public static ValidateObject StringChecks(this string value)
        {
            if (value.IsNullOrWhiteSpace())
                return new ValidateObject { Success = true };
            else
            {
                return StringCheck(value);
            }
        }

        ///// <summary>
        /////  判断是否为合法字符(a-zA-Z0-9-_)
        ///// </summary>
        public static ValidateObject IsRightfulString(this string value)
        {
            Regex isRightfulString = new Regex("^[A-Za-z0-9_-]+$");
            var ex = new ValidateObject();
            var m = isRightfulString.Match(value);
            ex.Success = m.Success;
            ex.Result = "请输入字母数字下划线";
            return ex;
        }
        public static ValidateObject IsRightfulStrings(this string value)
        {
            if (value.IsNullOrWhiteSpace())
                return new ValidateObject { Success = true };
            else
            {
                return IsRightfulString(value);
            }
        }

        /// <summary>
        /// 判断是否包含中英文特殊字符，除英文"-_"字符外
        /// </summary>
        public static ValidateObject IsContainsSpecialChar(this string value)
        {
            Regex isContainsSpecialChar = new Regex(@"[(\ )(\`)(\~)(\!)(\@)(\#)(\$)(\%)(\^)(\&)(\*)(\()(\))(\+)(\=)(\|)(\{)(\})(\')(\:)(\;)(\')(',)(\[)(\])(\.)(\<)(\>)(\/)(\?)(\~)(\！)(\@)(\#)(\￥)(\%)(\…)(\&)(\*)(\（)(\）)(\—)(\+)(\|)(\{)(\})(\【)(\】)(\‘)(\；)(\：)(\”)(\“)(\’)(\。)(\，)(\、)(\？)]+");
            var ex = new ValidateObject();
            var m = isContainsSpecialChar.Match(value);
            ex.Success = m.Success;
            ex.Result = "请输入中英文特殊字符，除英文'-_'字符外";
            return ex;
        }
        public static ValidateObject IsContainsSpecialChars(this string value)
        {
            if (value.IsNullOrWhiteSpace())
                return new ValidateObject { Success = true };
            else
            {
                return IsContainsSpecialChar(value);
            }

        }

        /// <summary>
        /// 邮箱校验
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static ValidateObject IsEmail(this string value)
        {
            var ex = new ValidateObject();
            Regex isEmail = new Regex(@"^[\w-]+(\.[\w-]+)*@[\w-]+(\.[\w-]+)+$");
            var m = isEmail.Match(value);
            ex.Success = m.Success;
            ex.Result = "请输入正确的邮箱";
            return ex;
        }

        public static ValidateObject IsEmails(this string value)
        {
            if (value.IsNullOrWhiteSpace())
                return new ValidateObject { Success = true };
            else
            {
                return IsEmail(value);
            }

        }
    }

    public class ValidateObject
    {
        public bool Success { get; set; }
        public string Result { get; set; }
    }
}

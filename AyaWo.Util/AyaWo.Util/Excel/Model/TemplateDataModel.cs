﻿
namespace AyaWo.Util
{
    /// <summary>
    /// 版 本 V.5.0.3
    /// Copyright (c) 2013-2017 
    /// 创建人：starryLuo
    /// 日 期：2017.03.04
    /// 描 述：模板数据模型
    /// </summary>
    public class TemplateDataModel
    {
        /// <summary>
        /// 行号
        /// </summary>
        public int row { get; set; }
        /// <summary>
        /// 列号
        /// </summary>
        public int cell { get; set; }
        /// <summary>
        /// 数据值
        /// </summary>
        public string value { get; set; }
    }
}

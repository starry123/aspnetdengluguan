﻿namespace AyaWo.Util
{
    /// <summary>
    /// 版 本 V.5.0.3
    /// Copyright (c) 2013-2017 
    /// 创建人：starryLuo
    /// 日 期：2017.03.07
    /// 描 述：数据库参数
    /// </summary>
    public class FieldValueParam
    {
        /// <summary>
        /// 字段名称
        /// </summary>
        public string name { get; set; }
        /// <summary>
        /// 数据值
        /// </summary>
        public object value { get; set; }
        /// <summary>
        /// 数据类型
        /// </summary>
        public int type { get; set; }
    }
}
